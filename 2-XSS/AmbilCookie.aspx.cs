﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _2_XSS
{
    public partial class AmbilCookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var cookie = Request.QueryString["cookie"];
            txtKeterangan.Text = "Berhasil mengambil data cookie " + cookie;
        }
    }
}